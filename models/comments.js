const mongoose = require('mongoose')
const { Schema } = mongoose


const opts = {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
}


const CommentssSchema = new Schema({
    content: String,
    author:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    }
}, opts)

module.exports = mongoose.model('comment', CommentssSchema)