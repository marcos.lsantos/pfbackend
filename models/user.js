const mongoose = require('mongoose')
const { Schema } = mongoose

const opts = {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
}

const UserSchema = new Schema({
    username: String,
    name: String,
    picture: String,
    is_admin: {
        type: Boolean,
        default: false
    },
    email:{
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        select: false
    }
}, opts)

module.exports = mongoose.model('user', UserSchema)