const mongoose = require("mongoose")
const {Schema} = mongoose

const opts ={
    createdAt: 'created_at',
    updatedAt: 'updated_at'
}

const BeachesSchema = new Schema({
    spot:{
        type: String,
        required: true
    },
    deep:{
        type: String,
        required: true
    },
    beach: {
        type: String,
        required: true
    },
    direction: {
        type: String,
        required: true
    },
    photo:[{
        type: String
    }],
    city:{
        type: String,
        required: true,
    },
    state:{
        type: String,
        required: true
    },
    dificulty:[{
        type: Number,
        enum: [1,2,3,4,5]
    }],
    crowd:[{
        type: Number,
        enum: [1,2,3,4,5]
    }],
    localismo:[{
        type: Number,
        enum: [1,2,3,4,5]
    }],
    frequencia:[{
        type: Number,
        enum: [1,2,3,4,5]
    }],
    comments:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'comment'
    }]
}, opts)

module.exports = mongoose.model('beaches', BeachesSchema)
