const express = require ('express')
var bodyparser = require('body-parser')
const app = express()
var cors = require('cors')
const fileUpload = require('express-fileupload')
const connectDB = require('./config/db')
const PORT = process.env.PORT || 3001


// Midlewares
app.use(express.json())
app.use(bodyparser.urlencoded({extended: true}))
app.use(bodyparser.json())
app.use(cors())
app.use('/uploads', express.static('uploads'))

app.use(fileUpload({
    createParentPath: true
}))

//Connect DB
connectDB()


// Routes
app.use('/', require('./routes/hello'))
app.use('/auth', require('./routes/api/auth'))
app.use('/user', require('./routes/api/user'))
app.use('/beaches', require('./routes/api/beaches'))
app.use('/comments', require('./routes/api/comments'))




app.listen(PORT, () => { console.log(`Porta ${PORT}`) })