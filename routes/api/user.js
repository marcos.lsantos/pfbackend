const express = require('express')
const router = express.Router()
const User = require('../../models/user')
const bcrypt = require('bcryptjs')
const auth = require('../../midleware/auth')
const file = require('../../midleware/file')
const {check, validationResult} = require('express-validator')



//@route    user/post
//@desc     create user with name, email and password
//@access   public
router.post('/',[
    check('email').isEmail(),
    check('password').isLength({min: 5}),
    check('name').not().isEmpty(),
    check('username').not().isEmpty()
], async (req, res, next) => {
    try{
        let password = req.body.password

        const errors = validationResult(req)

        if(!errors.isEmpty()){
            return res.status(400).json({errrors: errors.array()})
        }else{
            let usuario = new User(req.body)
            if(req.body.picture_name){
                usuario.picture = `user/${req.body.picture_name}`
            }
            const salt = await bcrypt.genSalt(10)
            usuario.password = await bcrypt.hash(password, salt)
            await usuario.save()
            if(usuario.id){
                res.json(usuario)
            }
        }
    }catch(err){
        res.status(500).send("Falta algum dado")
    }
})


router.delete('/:id', auth, async(req, res, next) =>{
    try{
        const id = req.params.id
        const user = await User.findOneAndDelete({_id: id})
        if(user){
            res.json("Usuário excluido")
        }else{
            res.json("Usuário não excluido")
        }
    }catch(err){
        console.error(err.messsage)
        res.status(500).send("Falta conteudo")
    }
})

router.get('/', auth, async(req, res, next) =>{
    try{
        const users = await User.find()
        if(users){
            res.json(users)
        }else{
            res.json("Não existem usuários")
        }
    }catch(err){
        console.error(err.messsage)
        res.status(500).send("Error")
    }
})

module.exports = router