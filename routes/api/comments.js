const express = require('express')
const router = express.Router()
const Beach = require('../../models/beaches')
const Comments = require ('../../models/comments')
const auth = require('../../midleware/auth')


router.post('/', auth, async(req, res, next) => {
    try{
        let id = req.body.beach
        req.body.author = req.user.id
        let comment = new Comments(req.body)
        await comment.save()

        const beach = await Beach.findOneAndUpdate({_id: id}, {$push: {comments: comment}}, {new: true})
        
        if(beach){
            res.json(beach)
        }else{
            res.status(404).send("Não Gravado")
        } 
    }catch(err){
        console.error(err.message)
    }
})


router.get('/', auth, async(req, res, next) => {
    try{
        const comments = await Comments.find({})
        res.json(comments)
    }catch(err){
        console.error(err.message)
    }
})

router.delete('/:id', auth, async(req, res, next) => {
    try{
        const id = req.params.id
        const comment = await Comments.findOneAndDelete({_id: id})
        if(comment){
            res.json("Comentario Excluido")
        }else{
            res.json("Algo deu errado")
        }
    }catch(err){
        console.error(err.message)
    }
})

module.exports = router