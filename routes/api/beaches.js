const express = require('express')
const router = express.Router()
const Beaches = require('../../models/beaches')
const { check } = require('express-validator')
const file = require('../../midleware/file')
const auth = require('../../midleware/auth')



router.post('/', [
    check('spot').not().isEmpty(),
    check('beach').not().isEmpty(),
    check('state').not().isEmpty(),
    check('city').not().isEmpty()
], auth, async(req, res, next) =>{
    try{
        let beaches = new Beaches(req.body)
        await beaches.save()
        if(beaches.id){
            res.json(beaches)
        }

    }catch(err){
        console.error(err.message)
        res.status(500).send("Faltando conteudo")
    }
})

router.get('/', async(req, res, next) =>{
    try{
        const beaches = await Beaches.find({})
        if(beaches){
            return res.json(beaches)
        }else{
            res.json("Não existem praias")
        }
    }catch(err){
        console.error(err.message)
    }
})

router.get('/:id', async (req, res, next) =>{
    try{
        const id = req.params.id
        const beach = await (await Beaches.findOne({ _id: id }).populate('comments'));
        if(beach){
            res.json(beach)
        }else{
            res.status(404).send("Id inexistente")
        }
    }catch(err){
        console.error(err.message)
    }
})

router.delete('/:id',auth,  async(req, res, next) =>{
    try{
        const id = req.params.id
        const beach =await Beaches.findOneAndDelete({_id: id})
        if(beach){
            res.send("Praia excluida!")
        }else{
            res.status(404).send("Exclusão não efetuada")
        }
    }catch(err){
        console.error(err.message)
        res.status(500).send("Error")
    }
})

module.exports = router
