const moongose = require('mongoose')
const config = require('config')

const connectDB = async(db=process.env.mongoURI || config.get('mongoURI')) =>{
    try{
        await moongose.connect(db,{
            useNewUrlParser: true,
			useCreateIndex: true,
			useFindAndModify: false,
			useUnifiedTopology: true
        })

        console.log('MongoDB Connected')
    }catch(err){
        console.error(err.message)
        process.exit(1)
    }
}

module.exports = connectDB